## Prepared (M)OFED libraries
MOFED and RDMA-Core user space libraries are available pre-packaged for use with `nventry` under ubuntu/16.04. All that is required to use these libraries is to copy the `/opt/ofed` directory from the `ofed` image, install `libnl`, and create an empty directory under `/etc/libibverbs.d`. The directories are required to suppress warnings from libibverbs.d constructors.

# Use in Dockerfile
```
COPY --from=registry.gitlab.com/nvhpc/ofed-builder:{VERSION} /opt/ofed /opt/ofed
apt-get install -y libnl-3-200 libnl-route-3-200
mkdir /etc/libibverbs.d
```
